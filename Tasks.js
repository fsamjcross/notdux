import Store from './Store.js'
import fetch from 'node-fetch'
import AppState from './App.js'

const Tasks = () =>{
		const state = Store(AppState,'Task')
		return{
				...state,
				get state(){return state},
				fetchTasks:async () =>{
						const raw = await fetch('https://randomuser.me/api?results=10')	
						const json = await raw.json()
						if(json.error) throw json.error
						state.setState('fetchTasks',{...state.state,results:json.results})
				}
		}
}
export default Tasks()
