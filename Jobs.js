import Store from './Store.js'
import fetch from 'node-fetch'
import Tasks from './Tasks.js'
const Jobs = () =>{
		const state = Store(Tasks,'job')
		return{
				...state,
				fetchTasks:async () =>{
						state.setState('fetchTasks',["job1","job2"])
				}
		}
}
export default Jobs()
