import app from './App.js'
export const history = [] 
export const startHistory = () => {
		app.on((action,state)=> history.push(
				{action,state,time: new Date().getTime()}
		))
}
 
export const rewind = () => {
		return history.pop()
}
