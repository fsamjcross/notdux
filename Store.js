const Store = (parent,name)=>{
		let state = parent.setState()
		const listeners = []
		
		return {
				setState(action,newState){
						state = newState
						this.fireListeners(action,state)
						return state
				},
				fireListeners(action,statein){
						const newState = {...state,[name]:statein}
						listeners.forEach(el => el(action,newState))
						parent.fireListeners(action,newState)
				},
				get state(){
						return state
				},
				on(fun){
						listeners.unshift(fun)	
				}
		}
}
export default Store
