import state from './Store.js'
let AppState = {}
let listeners = []
export default state({
		setState(state){AppState = state},
		get Store(){return AppState},
		fireListeners(action,state){
				listeners.forEach(el => el(action,state))
		},
		on(fun){
				listeners.unshift(fun)	
		}
},'App')
